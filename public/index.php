<?php

// Use Loader() to autoload our model
$loader = new \Phalcon\Loader();

$loader->registerDirs(array(
    __DIR__ . '/../models/'
))->register();

$di = new \Phalcon\DI\FactoryDefault();

//Set up the database service
$di->set('db', function(){
    return new \Phalcon\Db\Adapter\Pdo\Mysql([
        'host' => 'localhost',
        'username' => 'root',
        'password' => 'test',
        'dbname' => 'social_graph'
    ]);
});

$app = new \Phalcon\Mvc\Micro($di);

//Retrieves friends by user id
$app->get('/api/v1/friends/{id:[0-9]+}', function($id) use ($app)
{
    $phql = "SELECT Users.* FROM Users INNER JOIN UsersFriends ON Users.id = UsersFriends.friend_id  WHERE UsersFriends.user_id = :id:";
    $records = $app->modelsManager->executeQuery($phql, array(
        'id' => $id
    ));

    $data = array();
    foreach ($records as $row) {
        $data[] = array(
            "id" => $row->id,
            "firstName" => $row->firstName,
            "surname" => $row->surname,
            "gender" => $row->gender,
            "age" => $row->age
        );
    }

    echo json_encode($data);
});

//Retrieves friends of a friend by user id
$app->get('/api/v1/friends-of-friends/{id:[0-9]+}', function($id) use ($app) {

    $records = UsersFriends::getFriendsOfFriend($id);

    $data = array();
    foreach ($records as $row) {
        $data[] = array(
            "id" => $row->id,
            "firstName" => $row->firstName,
            "surname" => $row->surname,
            "gender" => $row->gender,
            "age" => $row->age
        );
    }

    echo json_encode($data);
});

$app->handle();
