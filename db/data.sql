INSERT INTO `social_graph`.`users` VALUES (1,'Paul','Crowe',28,'male'),(2,'Rob','Fitz',28,'male'),(3,'Ben','O\\\'Carolan',NULL,'male'),(4,'Victor',NULL,28,'male'),(5,'Peter','Mac',29,'male'),(6,'John','Barry',18,'male'),(7,'Sarah','Lane',30,'female'),(8,'Susan','Downe',28,'female'),(9,'Jack','Stam',28,'male'),(10,'Amy','Lane',24,'female'),(11,'Sandra','Phelan',28,'female'),(12,'Laura','Murphy',33,'female'),(13,'Lisa','Daly',28,'female'),(14,'Mark','Johnson',28,'male'),(15,'Seamus','Crowe',24,'male'),(16,'Daren','Slater',28,'male'),(17,'Dara','Zoltan',48,'male'),(18,'Marie','D',28,'female'),(19,'Catriona','Long',28,'female'),(20,'Katy','Couch',28,'female');
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (1, 2);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (2, 1);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (2, 3);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (3, 2);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (3, 4);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (3, 5);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (3, 7);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (4, 3);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (5, 3);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (5, 6);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (5, 11);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (5, 10);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (5, 7);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (6, 5);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (7, 3);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (7, 5);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (7, 20);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (7, 12);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (7, 8);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (8, 7);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (9, 12);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (10, 5);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (10, 11);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (11, 5);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (11, 10);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (11, 19);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (11, 20);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (12, 7);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (12, 9);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (12, 13);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (12, 20);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (13, 12);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (13, 14);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (13, 20);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (14, 13);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (14, 15);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (15, 14);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (16, 18);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (16, 20);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (17, 18);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (17, 20);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (18, 17);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (19, 11);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (19, 20);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (20, 7);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (20, 11);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (20, 12);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (20, 13);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (20, 16);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (20, 17);
INSERT INTO `social_graph`.`users_friends` (`user_id`, `friend_id`) VALUES (20, 19);