Social graph demo
=================

Social graph demo using Phalcon framework

* Direct friends: Return those people who are directly connected to the chosen person.
* Friends of friends: Return those who are two steps away, but not directly connected to the chosen person.

Install
=================
Install phalcon extension. Find it on

https://github.com/phalcon/cphalcon

Change db connection settings in public/index.php.
Create database with use of database.sql in db folder.
Import demo data in data.sql.

Access demo data
=================

* Direct friends:

Accessing friends of user whose id is 2

http://social-graph/api/v1/friends/2

* Friends of friends:

Accessing friends of friends of the user by user id 10

http://social-graph/api/v1/friends-of-friends/10

