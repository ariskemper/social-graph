<?php

class Users extends \Phalcon\Mvc\Model
{
    public $id;

    public $firstName;

    public $surname;

    public $age;

    public $gender;

    /**
     * This model is mapped to the table sample_cars
     */
    public function getSource()
    {
        return 'users';
    }

    public function initialize()
    {
        $this->hasMany('id', 'UsersFriends', 'user_id');
    }
}
