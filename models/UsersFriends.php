<?php

use Phalcon\Mvc\Model\Resultset\Simple as Resultset;

class UsersFriends extends \Phalcon\Mvc\Model
{
    public $user_id;

    public $friend_id;

    public function getSource()
    {
        return 'users_friends';
    }

    public function initialize()
    {
        $this->belongsTo('user_id', 'Users', 'id');
        $this->belongsTo('friend_id', 'Users', 'id');
    }

    /**
     * @param $id
     * @return Resultset
     */
    public function getFriendsOfFriend($id)
    {
        $sql = "SELECT *
                FROM users
                AS u
                LEFT JOIN users_friends
                AS uf
                ON u.id = uf.friend_id
                WHERE user_id
                IN (
                    SELECT friend_id FROM users_friends WHERE user_id = :id
                )
                AND
                    friend_id
                NOT IN (
                    SELECT friend_id
                    FROM users_friends
                    WHERE user_id = :id
                )
                AND friend_id <> :id
                GROUP BY friend_id";

        $usersFriends= new UsersFriends();
        return new Resultset(null, $usersFriends, $usersFriends->getReadConnection()->query($sql, array('id' => $id)));
    }
}
